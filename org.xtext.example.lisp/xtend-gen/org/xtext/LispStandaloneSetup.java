/**
 * generated by Xtext 2.15.0
 */
package org.xtext;

import org.xtext.LispStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
@SuppressWarnings("all")
public class LispStandaloneSetup extends LispStandaloneSetupGenerated {
  public static void doSetup() {
    new LispStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
