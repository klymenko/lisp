/*
 * generated by Xtext 2.15.0
 */
grammar InternalLisp;

options {
	superClass=AbstractInternalContentAssistParser;
}

@lexer::header {
package org.xtext.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.services.LispGrammarAccess;

}
@parser::members {
	private LispGrammarAccess grammarAccess;

	public void setGrammarAccess(LispGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}

	@Override
	protected Grammar getGrammar() {
		return grammarAccess.getGrammar();
	}

	@Override
	protected String getValueForTokenName(String tokenName) {
		return tokenName;
	}
}

// Entry rule entryRuleDomainmodel
entryRuleDomainmodel
:
{ before(grammarAccess.getDomainmodelRule()); }
	 ruleDomainmodel
{ after(grammarAccess.getDomainmodelRule()); } 
	 EOF 
;

// Rule Domainmodel
ruleDomainmodel 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		(
			{ before(grammarAccess.getDomainmodelAccess().getExpressionsAssignment()); }
			(rule__Domainmodel__ExpressionsAssignment)
			{ after(grammarAccess.getDomainmodelAccess().getExpressionsAssignment()); }
		)
		(
			{ before(grammarAccess.getDomainmodelAccess().getExpressionsAssignment()); }
			(rule__Domainmodel__ExpressionsAssignment)*
			{ after(grammarAccess.getDomainmodelAccess().getExpressionsAssignment()); }
		)
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleExpression
entryRuleExpression
:
{ before(grammarAccess.getExpressionRule()); }
	 ruleExpression
{ after(grammarAccess.getExpressionRule()); } 
	 EOF 
;

// Rule Expression
ruleExpression 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getExpressionAccess().getAlternatives()); }
		(rule__Expression__Alternatives)
		{ after(grammarAccess.getExpressionAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleArithmeticExpression
entryRuleArithmeticExpression
:
{ before(grammarAccess.getArithmeticExpressionRule()); }
	 ruleArithmeticExpression
{ after(grammarAccess.getArithmeticExpressionRule()); } 
	 EOF 
;

// Rule ArithmeticExpression
ruleArithmeticExpression 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getAlternatives()); }
		(rule__ArithmeticExpression__Alternatives)
		{ after(grammarAccess.getArithmeticExpressionAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleMinus
entryRuleMinus
:
{ before(grammarAccess.getMinusRule()); }
	 ruleMinus
{ after(grammarAccess.getMinusRule()); } 
	 EOF 
;

// Rule Minus
ruleMinus 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getMinusAccess().getGroup()); }
		(rule__Minus__Group__0)
		{ after(grammarAccess.getMinusAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleDiv
entryRuleDiv
:
{ before(grammarAccess.getDivRule()); }
	 ruleDiv
{ after(grammarAccess.getDivRule()); } 
	 EOF 
;

// Rule Div
ruleDiv 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getDivAccess().getGroup()); }
		(rule__Div__Group__0)
		{ after(grammarAccess.getDivAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleDefvar
entryRuleDefvar
:
{ before(grammarAccess.getDefvarRule()); }
	 ruleDefvar
{ after(grammarAccess.getDefvarRule()); } 
	 EOF 
;

// Rule Defvar
ruleDefvar 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getDefvarAccess().getGroup()); }
		(rule__Defvar__Group__0)
		{ after(grammarAccess.getDefvarAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRulePlus
entryRulePlus
:
{ before(grammarAccess.getPlusRule()); }
	 rulePlus
{ after(grammarAccess.getPlusRule()); } 
	 EOF 
;

// Rule Plus
rulePlus 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getPlusAccess().getGroup()); }
		(rule__Plus__Group__0)
		{ after(grammarAccess.getPlusAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleMul
entryRuleMul
:
{ before(grammarAccess.getMulRule()); }
	 ruleMul
{ after(grammarAccess.getMulRule()); } 
	 EOF 
;

// Rule Mul
ruleMul 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getMulAccess().getGroup()); }
		(rule__Mul__Group__0)
		{ after(grammarAccess.getMulAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleList
entryRuleList
:
{ before(grammarAccess.getListRule()); }
	 ruleList
{ after(grammarAccess.getListRule()); } 
	 EOF 
;

// Rule List
ruleList 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getListAccess().getGroup()); }
		(rule__List__Group__0)
		{ after(grammarAccess.getListAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleEcho
entryRuleEcho
:
{ before(grammarAccess.getEchoRule()); }
	 ruleEcho
{ after(grammarAccess.getEchoRule()); } 
	 EOF 
;

// Rule Echo
ruleEcho 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getEchoAccess().getGroup()); }
		(rule__Echo__Group__0)
		{ after(grammarAccess.getEchoAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleCDR
entryRuleCDR
:
{ before(grammarAccess.getCDRRule()); }
	 ruleCDR
{ after(grammarAccess.getCDRRule()); } 
	 EOF 
;

// Rule CDR
ruleCDR 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getCDRAccess().getGroup()); }
		(rule__CDR__Group__0)
		{ after(grammarAccess.getCDRAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleCAR
entryRuleCAR
:
{ before(grammarAccess.getCARRule()); }
	 ruleCAR
{ after(grammarAccess.getCARRule()); } 
	 EOF 
;

// Rule CAR
ruleCAR 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getCARAccess().getGroup()); }
		(rule__CAR__Group__0)
		{ after(grammarAccess.getCARAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleNIL
entryRuleNIL
:
{ before(grammarAccess.getNILRule()); }
	 ruleNIL
{ after(grammarAccess.getNILRule()); } 
	 EOF 
;

// Rule NIL
ruleNIL 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getNILAccess().getNilKeyword()); }
		'nil'
		{ after(grammarAccess.getNILAccess().getNilKeyword()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getDefvarAssignment_0()); }
		(rule__Expression__DefvarAssignment_0)
		{ after(grammarAccess.getExpressionAccess().getDefvarAssignment_0()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getArithmeticExpressionParserRuleCall_1()); }
		ruleArithmeticExpression
		{ after(grammarAccess.getExpressionAccess().getArithmeticExpressionParserRuleCall_1()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getListAssignment_2()); }
		(rule__Expression__ListAssignment_2)
		{ after(grammarAccess.getExpressionAccess().getListAssignment_2()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getCdrAssignment_3()); }
		(rule__Expression__CdrAssignment_3)
		{ after(grammarAccess.getExpressionAccess().getCdrAssignment_3()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getCarAssignment_4()); }
		(rule__Expression__CarAssignment_4)
		{ after(grammarAccess.getExpressionAccess().getCarAssignment_4()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getEchoAssignment_5()); }
		(rule__Expression__EchoAssignment_5)
		{ after(grammarAccess.getExpressionAccess().getEchoAssignment_5()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getNilAssignment_6()); }
		(rule__Expression__NilAssignment_6)
		{ after(grammarAccess.getExpressionAccess().getNilAssignment_6()); }
	)
	|
	(
		{ before(grammarAccess.getExpressionAccess().getStrAssignment_7()); }
		(rule__Expression__StrAssignment_7)
		{ after(grammarAccess.getExpressionAccess().getStrAssignment_7()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__ArithmeticExpression__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getPlusAssignment_0()); }
		(rule__ArithmeticExpression__PlusAssignment_0)
		{ after(grammarAccess.getArithmeticExpressionAccess().getPlusAssignment_0()); }
	)
	|
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getMinusAssignment_1()); }
		(rule__ArithmeticExpression__MinusAssignment_1)
		{ after(grammarAccess.getArithmeticExpressionAccess().getMinusAssignment_1()); }
	)
	|
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getMulAssignment_2()); }
		(rule__ArithmeticExpression__MulAssignment_2)
		{ after(grammarAccess.getArithmeticExpressionAccess().getMulAssignment_2()); }
	)
	|
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getDivAssignment_3()); }
		(rule__ArithmeticExpression__DivAssignment_3)
		{ after(grammarAccess.getArithmeticExpressionAccess().getDivAssignment_3()); }
	)
	|
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getIntAssignment_4()); }
		(rule__ArithmeticExpression__IntAssignment_4)
		{ after(grammarAccess.getArithmeticExpressionAccess().getIntAssignment_4()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Minus__Group__0__Impl
	rule__Minus__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMinusAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getMinusAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Minus__Group__1__Impl
	rule__Minus__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMinusAccess().getHyphenMinusKeyword_1()); }
	'-'
	{ after(grammarAccess.getMinusAccess().getHyphenMinusKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Minus__Group__2__Impl
	rule__Minus__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMinusAccess().getEl1Assignment_2()); }
	(rule__Minus__El1Assignment_2)
	{ after(grammarAccess.getMinusAccess().getEl1Assignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Minus__Group__3__Impl
	rule__Minus__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMinusAccess().getEl2Assignment_3()); }
	(rule__Minus__El2Assignment_3)
	{ after(grammarAccess.getMinusAccess().getEl2Assignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Minus__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMinusAccess().getRightParenthesisKeyword_4()); }
	')'
	{ after(grammarAccess.getMinusAccess().getRightParenthesisKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Div__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Div__Group__0__Impl
	rule__Div__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDivAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getDivAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Div__Group__1__Impl
	rule__Div__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDivAccess().getSolidusKeyword_1()); }
	'/'
	{ after(grammarAccess.getDivAccess().getSolidusKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Div__Group__2__Impl
	rule__Div__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDivAccess().getEl1Assignment_2()); }
	(rule__Div__El1Assignment_2)
	{ after(grammarAccess.getDivAccess().getEl1Assignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Div__Group__3__Impl
	rule__Div__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDivAccess().getEl2Assignment_3()); }
	(rule__Div__El2Assignment_3)
	{ after(grammarAccess.getDivAccess().getEl2Assignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Div__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDivAccess().getRightParenthesisKeyword_4()); }
	')'
	{ after(grammarAccess.getDivAccess().getRightParenthesisKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Defvar__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Defvar__Group__0__Impl
	rule__Defvar__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDefvarAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getDefvarAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Defvar__Group__1__Impl
	rule__Defvar__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDefvarAccess().getDefvarKeyword_1()); }
	'defvar'
	{ after(grammarAccess.getDefvarAccess().getDefvarKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Defvar__Group__2__Impl
	rule__Defvar__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDefvarAccess().getNameAssignment_2()); }
	(rule__Defvar__NameAssignment_2)
	{ after(grammarAccess.getDefvarAccess().getNameAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Defvar__Group__3__Impl
	rule__Defvar__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDefvarAccess().getExpressionAssignment_3()); }
	(rule__Defvar__ExpressionAssignment_3)?
	{ after(grammarAccess.getDefvarAccess().getExpressionAssignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Defvar__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getDefvarAccess().getRightParenthesisKeyword_4()); }
	')'
	{ after(grammarAccess.getDefvarAccess().getRightParenthesisKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Plus__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Plus__Group__0__Impl
	rule__Plus__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPlusAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getPlusAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Plus__Group__1__Impl
	rule__Plus__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPlusAccess().getPlusSignKeyword_1()); }
	'+'
	{ after(grammarAccess.getPlusAccess().getPlusSignKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Plus__Group__2__Impl
	rule__Plus__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPlusAccess().getEl1Assignment_2()); }
	(rule__Plus__El1Assignment_2)
	{ after(grammarAccess.getPlusAccess().getEl1Assignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Plus__Group__3__Impl
	rule__Plus__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPlusAccess().getEl2Assignment_3()); }
	(rule__Plus__El2Assignment_3)
	{ after(grammarAccess.getPlusAccess().getEl2Assignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Plus__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPlusAccess().getRightParenthesisKeyword_4()); }
	')'
	{ after(grammarAccess.getPlusAccess().getRightParenthesisKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Mul__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Mul__Group__0__Impl
	rule__Mul__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMulAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getMulAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Mul__Group__1__Impl
	rule__Mul__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMulAccess().getAsteriskKeyword_1()); }
	'*'
	{ after(grammarAccess.getMulAccess().getAsteriskKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Mul__Group__2__Impl
	rule__Mul__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMulAccess().getEl1Assignment_2()); }
	(rule__Mul__El1Assignment_2)
	{ after(grammarAccess.getMulAccess().getEl1Assignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Mul__Group__3__Impl
	rule__Mul__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMulAccess().getEl2Assignment_3()); }
	(rule__Mul__El2Assignment_3)
	{ after(grammarAccess.getMulAccess().getEl2Assignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Mul__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getMulAccess().getRightParenthesisKeyword_4()); }
	')'
	{ after(grammarAccess.getMulAccess().getRightParenthesisKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__List__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__List__Group__0__Impl
	rule__List__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getListAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getListAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__List__Group__1__Impl
	rule__List__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getListAccess().getListKeyword_1()); }
	'list'
	{ after(grammarAccess.getListAccess().getListKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__List__Group__2__Impl
	rule__List__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	(
		{ before(grammarAccess.getListAccess().getExpressionAssignment_2()); }
		(rule__List__ExpressionAssignment_2)
		{ after(grammarAccess.getListAccess().getExpressionAssignment_2()); }
	)
	(
		{ before(grammarAccess.getListAccess().getExpressionAssignment_2()); }
		(rule__List__ExpressionAssignment_2)*
		{ after(grammarAccess.getListAccess().getExpressionAssignment_2()); }
	)
)
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__List__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__List__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getListAccess().getRightParenthesisKeyword_3()); }
	')'
	{ after(grammarAccess.getListAccess().getRightParenthesisKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Echo__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Echo__Group__0__Impl
	rule__Echo__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getEchoAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getEchoAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Echo__Group__1__Impl
	rule__Echo__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getEchoAccess().getEchoKeyword_1()); }
	'echo'
	{ after(grammarAccess.getEchoAccess().getEchoKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Echo__Group__2__Impl
	rule__Echo__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	(
		{ before(grammarAccess.getEchoAccess().getExpressionAssignment_2()); }
		(rule__Echo__ExpressionAssignment_2)
		{ after(grammarAccess.getEchoAccess().getExpressionAssignment_2()); }
	)
	(
		{ before(grammarAccess.getEchoAccess().getExpressionAssignment_2()); }
		(rule__Echo__ExpressionAssignment_2)*
		{ after(grammarAccess.getEchoAccess().getExpressionAssignment_2()); }
	)
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Echo__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getEchoAccess().getRightParenthesisKeyword_3()); }
	')'
	{ after(grammarAccess.getEchoAccess().getRightParenthesisKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__CDR__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CDR__Group__0__Impl
	rule__CDR__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCDRAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getCDRAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CDR__Group__1__Impl
	rule__CDR__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCDRAccess().getCdrKeyword_1()); }
	'cdr'
	{ after(grammarAccess.getCDRAccess().getCdrKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CDR__Group__2__Impl
	rule__CDR__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCDRAccess().getExpressionAssignment_2()); }
	(rule__CDR__ExpressionAssignment_2)
	{ after(grammarAccess.getCDRAccess().getExpressionAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CDR__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCDRAccess().getRightParenthesisKeyword_3()); }
	')'
	{ after(grammarAccess.getCDRAccess().getRightParenthesisKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__CAR__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CAR__Group__0__Impl
	rule__CAR__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCARAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getCARAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CAR__Group__1__Impl
	rule__CAR__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCARAccess().getCarKeyword_1()); }
	'car'
	{ after(grammarAccess.getCARAccess().getCarKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CAR__Group__2__Impl
	rule__CAR__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCARAccess().getExpressionAssignment_2()); }
	(rule__CAR__ExpressionAssignment_2)
	{ after(grammarAccess.getCARAccess().getExpressionAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__CAR__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCARAccess().getRightParenthesisKeyword_3()); }
	')'
	{ after(grammarAccess.getCARAccess().getRightParenthesisKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Domainmodel__ExpressionsAssignment
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getDomainmodelAccess().getExpressionsExpressionParserRuleCall_0()); }
		ruleExpression
		{ after(grammarAccess.getDomainmodelAccess().getExpressionsExpressionParserRuleCall_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__DefvarAssignment_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getDefvarDefvarParserRuleCall_0_0()); }
		ruleDefvar
		{ after(grammarAccess.getExpressionAccess().getDefvarDefvarParserRuleCall_0_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__ListAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getListListParserRuleCall_2_0()); }
		ruleList
		{ after(grammarAccess.getExpressionAccess().getListListParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__CdrAssignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getCdrCDRParserRuleCall_3_0()); }
		ruleCDR
		{ after(grammarAccess.getExpressionAccess().getCdrCDRParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__CarAssignment_4
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getCarCARParserRuleCall_4_0()); }
		ruleCAR
		{ after(grammarAccess.getExpressionAccess().getCarCARParserRuleCall_4_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__EchoAssignment_5
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getEchoEchoParserRuleCall_5_0()); }
		ruleEcho
		{ after(grammarAccess.getExpressionAccess().getEchoEchoParserRuleCall_5_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__NilAssignment_6
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getNilNILParserRuleCall_6_0()); }
		ruleNIL
		{ after(grammarAccess.getExpressionAccess().getNilNILParserRuleCall_6_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Expression__StrAssignment_7
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExpressionAccess().getStrSTRINGTerminalRuleCall_7_0()); }
		RULE_STRING
		{ after(grammarAccess.getExpressionAccess().getStrSTRINGTerminalRuleCall_7_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__ArithmeticExpression__PlusAssignment_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getPlusPlusParserRuleCall_0_0()); }
		rulePlus
		{ after(grammarAccess.getArithmeticExpressionAccess().getPlusPlusParserRuleCall_0_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__ArithmeticExpression__MinusAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getMinusMinusParserRuleCall_1_0()); }
		ruleMinus
		{ after(grammarAccess.getArithmeticExpressionAccess().getMinusMinusParserRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__ArithmeticExpression__MulAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getMulMulParserRuleCall_2_0()); }
		ruleMul
		{ after(grammarAccess.getArithmeticExpressionAccess().getMulMulParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__ArithmeticExpression__DivAssignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getDivDivParserRuleCall_3_0()); }
		ruleDiv
		{ after(grammarAccess.getArithmeticExpressionAccess().getDivDivParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__ArithmeticExpression__IntAssignment_4
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getArithmeticExpressionAccess().getIntINTTerminalRuleCall_4_0()); }
		RULE_INT
		{ after(grammarAccess.getArithmeticExpressionAccess().getIntINTTerminalRuleCall_4_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__El1Assignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getMinusAccess().getEl1ExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getMinusAccess().getEl1ExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Minus__El2Assignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getMinusAccess().getEl2ExpressionParserRuleCall_3_0()); }
		ruleExpression
		{ after(grammarAccess.getMinusAccess().getEl2ExpressionParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__El1Assignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getDivAccess().getEl1ExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getDivAccess().getEl1ExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Div__El2Assignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getDivAccess().getEl2ExpressionParserRuleCall_3_0()); }
		ruleExpression
		{ after(grammarAccess.getDivAccess().getEl2ExpressionParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__NameAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getDefvarAccess().getNameIDTerminalRuleCall_2_0()); }
		RULE_ID
		{ after(grammarAccess.getDefvarAccess().getNameIDTerminalRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Defvar__ExpressionAssignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getDefvarAccess().getExpressionExpressionParserRuleCall_3_0()); }
		ruleExpression
		{ after(grammarAccess.getDefvarAccess().getExpressionExpressionParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__El1Assignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getPlusAccess().getEl1ExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getPlusAccess().getEl1ExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Plus__El2Assignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getPlusAccess().getEl2ExpressionParserRuleCall_3_0()); }
		ruleExpression
		{ after(grammarAccess.getPlusAccess().getEl2ExpressionParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__El1Assignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getMulAccess().getEl1ExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getMulAccess().getEl1ExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Mul__El2Assignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getMulAccess().getEl2ExpressionParserRuleCall_3_0()); }
		ruleExpression
		{ after(grammarAccess.getMulAccess().getEl2ExpressionParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__List__ExpressionAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getListAccess().getExpressionExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getListAccess().getExpressionExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Echo__ExpressionAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getEchoAccess().getExpressionExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getEchoAccess().getExpressionExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__CDR__ExpressionAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getCDRAccess().getExpressionExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getCDRAccess().getExpressionExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__CAR__ExpressionAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getCARAccess().getExpressionExpressionParserRuleCall_2_0()); }
		ruleExpression
		{ after(grammarAccess.getCARAccess().getExpressionExpressionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
