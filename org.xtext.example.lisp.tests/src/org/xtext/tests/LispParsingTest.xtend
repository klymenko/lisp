/*
 * generated by Xtext 2.15.0
 */
package org.xtext.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.xtext.lisp.Domainmodel
import org.eclipse.xtext.testing.extensions.InjectionExtension

@ExtendWith(InjectionExtension)
@InjectWith(LispInjectorProvider)
class LispParsingTest {
	@Inject
	ParseHelper<Domainmodel> parseHelper
	
	@Test
	def void testPlusPositive() {
		val result = parseHelper.parse('''
			(+ 1 1)
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
	
	@Test
	def void testPlusNegative() {
		var result = parseHelper.parse('''
			(+ 1)
		''')
		Assertions.assertNotNull(result)
		var errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
		
		result = parseHelper.parse('''
			(+ 1 1 1)
		''')
		Assertions.assertNotNull(result)
		errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
	}
	
	@Test
	def void testMinusPositive() {
		val result = parseHelper.parse('''
			(- 1 1)
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
	
	@Test
	def void testMinusNegative() {
		var result = parseHelper.parse('''
			(- 1)
		''')
		Assertions.assertNotNull(result)
		var errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
		
		result = parseHelper.parse('''
			(- 1 1 1)
		''')
		Assertions.assertNotNull(result)
		errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
	}
	
	@Test
	def void testMultiplyPositive() {
		val result = parseHelper.parse('''
			(* 1 1)
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
	
	@Test
	def void testMultiplyNegative() {
		var result = parseHelper.parse('''
			(* 1)
		''')
		Assertions.assertNotNull(result)
		var errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
		
		result = parseHelper.parse('''
			(* 1 1 1)
		''')
		Assertions.assertNotNull(result)
		errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
	}
	
	@Test
	def void testDividePositive() {
		val result = parseHelper.parse('''
			(/ 1 1)
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
	
	@Test
	def void testDivideNegative() {
		var result = parseHelper.parse('''
			(/ 1)
		''')
		Assertions.assertNotNull(result)
		var errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
		
		result = parseHelper.parse('''
			(/ 1 1 1)
		''')
		Assertions.assertNotNull(result)
		errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty)
	}
	
	@Test
	def void testListPositive() {
		val result = parseHelper.parse('''
			(list 1)
			(list 1 1)
			(list 1 (+ 1 1))
			(list (* 1 1) (- 1 1))
			(car (list (* 1 1) (- 1 1)))
			(cdr (list (* 1 1) (- 1 1)))
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
	
	@Test
	def void testEchoPositive() {
		val result = parseHelper.parse('''
			(echo 1)
			(echo (cdr (list (* 1 1) (- 1 1))))
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
	
	@Test
	def void testDefvarPositive() {
		val result = parseHelper.parse('''
			(defvar test 1)
			(defvar test (cdr (list (* 1 1) (- 1 1))))
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		println(errors.join(", "))
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
	}
}
